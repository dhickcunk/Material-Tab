package com.dhickcunk.materialtab.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dhickcunk.materialtab.fragment.LatihanFragment;
import com.dhickcunk.materialtab.fragment.HitungLuasFragment;
import com.dhickcunk.materialtab.fragment.SampleIntentFragment;

public class TabFragmentPagerAdapter extends FragmentPagerAdapter {

    //nama tab nya
    String[] title = new String[]{
            "Hitung Luas", "Latihan Intent", "Tab 3"
    };

    public TabFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    //method ini yang akan memanipulasi penampilan Fragment dilayar
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new HitungLuasFragment();
                break;
            case 1:
                fragment = new SampleIntentFragment();
                break;
            case 2:
                fragment = new LatihanFragment();
                break;
            default:
                fragment = null;
                break;
        }

        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public int getCount() {
        return title.length;
    }
}